#!/usr/bin/env python

import sys
import os
import stat

script = sys.argv[1]

fscript = file(script)
content = [x for x in fscript.readlines() if not x.startswith("#!")
                                        and "-*- coding:" not in x]
content.insert(0, "# -*- coding: utf-8 -*-\n")
content.insert(0, "#!/usr/bin/env python\n")
fscript.close()

fscript = file(script, "w")
fscript.writelines(content)
fscript.close()

os.chmod(script, stat.S_IRWXU | stat.S_IRGRP |
         stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)
